// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const baseUrlApi = "https://127.0.0.1:8000/api/";
export const environment = {
  production: false,
  //>Route api
    apiUrlLogin : baseUrlApi + "login_check",
    apiUrlRefreshToken : baseUrlApi + "token/refresh",
    apiUrlAdvertisement : baseUrlApi + "advertisements/",
    apiUrlUser : baseUrlApi + "users/",
    apiUrlProfessional : baseUrlApi + "professionals/",
    apiUrlGarage : baseUrlApi + "garages/",
    apiUrldrivers : baseUrlApi + "drivers/",
    apiUrlCity : baseUrlApi + "cities/",
    apiUrlBrand : baseUrlApi + "brands/",
    apiUrlModel : baseUrlApi + "models/",
    apiUrlFile: baseUrlApi + "files/",
    apiUrlCar : baseUrlApi + "cars/",
    apiUrMaintenance : baseUrlApi + "maintenances/",
  //<Route Api

  //>pathApp
    homePath : "home",
    loginPath : "login",
    loginRegister : "register",
  //<pathApp

  //>token
    authToken: "cookieStorageToken",
    refreshToken: "refreshToken",
  //<token

  //>roles
    roleAdmin : "ROLE_ADMIN",
    roleProfessional : "ROLE_PRO",
    roleDriver: "ROLE_DIRVER",
    //<roles
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
