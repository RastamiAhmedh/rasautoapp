export class DecodeToken {

    iat: number;
    exp: number;
    roles: Array<string>;
    username: string;
}
