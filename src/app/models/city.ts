import { Base } from "./base";

export class City extends Base {
    id:number;
    name:string;
    postcode:string;
}
