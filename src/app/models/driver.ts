import { Car } from "./car";
import { Choice } from "./choice";
import { City } from "./city";
import { File } from "./file";
import { User } from "./user";

export class Driver {
    id:number;
    phone:string;
    address: string;
    addional_address:string;
    city : City;
    files : Array<File>;
    cars: Array<Car>;
    choices: Array<Choice>;
    user: User;

}
