import { Garage } from "./garage";
import { User } from "./user";

export class Professional {
    id:number;
    siret:string;
    user: User;
    garages: Array<Garage>;
}