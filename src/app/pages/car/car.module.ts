import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarPageRoutingModule } from './car-routing.module';

import { CarPage } from './car.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { NavMenuComponent } from 'src/app/components/nav-menu/nav-menu.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarPageRoutingModule
  ],
  declarations: [CarPage,HeaderComponent,NavMenuComponent]
})
export class CarPageModule {}
