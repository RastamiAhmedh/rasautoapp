import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChoicesPageRoutingModule } from './choices-routing.module';

import { ChoicesPage } from './choices.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { NavMenuComponent } from 'src/app/components/nav-menu/nav-menu.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChoicesPageRoutingModule
  ],
  declarations: [ChoicesPage,HeaderComponent,NavMenuComponent]
})
export class ChoicesPageModule {}
