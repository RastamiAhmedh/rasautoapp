import {Component, OnInit} from "@angular/core";
import { Router } from "@angular/router";
import { Driver } from "src/app/models/driver";
import {Professional} from "src/app/models/professional";
import {User} from "src/app/models/user";
import { CityService } from "src/app/services/city/city.service";
import { DriverService } from "src/app/services/driver/driver.service";
import { ProfessionalService } from "src/app/services/professional/professional.service";
import { ToasterService } from "src/app/services/toast/toaster.service";

@Component({selector: "app-register", templateUrl: "./register.page.html", styleUrls: ["./register.page.scss"]})
export class RegisterPage implements OnInit {
  selectedAccount: boolean;
  typeAccount: string;
  isPro: boolean;
  isDriver: boolean;
  fromInvalid;
  cities;
  isLoadin:boolean;
  constructor(
    private ProfesionalService: ProfessionalService,
    private driverService: DriverService,
    private cityService:CityService, 
    private toasterService: ToasterService,
    private router : Router) {}

  ngOnInit() {
    this.isLoadin = true;
    this.cityService.getAll().subscribe(
      res => {
        this.cities = res;
        this.isLoadin = false;
      },
      err => {
        this.toasterService.presentToast("City doesn't load","danger",2000);
        this.isLoadin = false;
      }
      );
    this.selectedAccount = false;
  }
  account(formA) {
    this.typeAccount = formA.value.type;
    this.isPro = this.typeAccount == "pro";
    this.isDriver = this.typeAccount == "driver";
    this.selectedAccount = true;
  }
  confirmeChange(form) {
    if (form.value.confirm != form.value.password) {
      this.fromInvalid = true;
    } else {
      this.fromInvalid = false;
    }
  }
  add(form) {
    let user = this.setUser(form);
    if(this.isPro){
      let pro = new Professional();
      pro.siret = form.value.siret;
      pro.user = user;
      this.ProfesionalService.post(pro).subscribe(
        res =>{
          this.toasterService.presentToast("Your account is successfully created, consult your email","success",6000);
          this.router.navigate(["/home"]);
        },
        err =>{
          let mess = "error";
          if ( err.error instanceof Object) {
            for (const prop in err.error){
              mess = err.error[prop] + ",  "
            }
          }else{
            mess = err.error;
          }
          this.toasterService.presentToast(mess,"danger",5000);
        }
      );  
    }
    if(this.isDriver){
      let driver = new Driver;
      driver.address = form.value.address;
      driver.addional_address = form.value.addional_address;
      driver.phone = form.value.phone;
      driver.city = form.value.city;
      driver.user = user;
      this.driverService.post(driver).subscribe(
        res =>{
          this.toasterService.presentToast("Your account is successfully created, consult your email","success",6000);
          this.router.navigate(["/home"]);
        },
        err =>{
          let mess = "error";
          if ( err.error instanceof Object) {
            for (const prop in err.error){
              mess = err.error[prop] + ",  "
            }
          }else{
            mess = err.error;
          }
          this.toasterService.presentToast(mess,"danger",5000);
        }
      );  
    }
  }

  setUser(form):User{
    let user = new User();
    user.email = form.value.email;
    user.firstname = form.value.firstname;
    user.lastname = form.value.lastname;
    user.password = form.value.password;
    user.username = form.value.username;
    return user;
  }
}
