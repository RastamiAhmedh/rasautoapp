import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ToasterService } from 'src/app/services/toast/toaster.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  isLoading: boolean;
  error = false;
  messageErr: any;
  constructor(
    private  authService:  AuthService, 
    private  router:  Router, 
    private cookieService : CookieService,) { }

  ngOnInit() {
    if(this.authService.isLogged()){
      this.router.navigate(['/home']);
    }
  }
  login(form){

    this.isLoading = true;
    this.authService.login(form.value).subscribe(
      (res)=>{
        this.error = false;
        this.cookieService.set(environment.authToken,res.token);
        this.cookieService.set(environment.refreshToken,res.refresh_token);
        this.router.navigateByUrl(environment.homePath);
    },
    err =>{
        this.error = true;
        this.messageErr = err.error.message;
        this.isLoading = false;
      
    });
  }
}
