import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfessionalPageRoutingModule } from './professional-routing.module';

import { ProfessionalPage } from './professional.page';
import { NavMenuComponent } from 'src/app/components/nav-menu/nav-menu.component';
import { HeaderComponent } from 'src/app/components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfessionalPageRoutingModule
  ],
  declarations: [ProfessionalPage,HeaderComponent,NavMenuComponent]
})
export class ProfessionalPageModule {}
