import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdvertisementsPageRoutingModule } from './advertisements-routing.module';

import { AdvertisementsPage } from './advertisements.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { NavMenuComponent } from 'src/app/components/nav-menu/nav-menu.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdvertisementsPageRoutingModule
  ],
  declarations: [AdvertisementsPage,HeaderComponent,NavMenuComponent]
})
export class AdvertisementsPageModule {}
