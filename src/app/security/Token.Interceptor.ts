import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/internal/operators';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { CookieService } from 'ngx-cookie-service';
import { LoginUser } from '../models/login-user';
import { AuthService } from '../services/auth/auth.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor{

    constructor(private router: Router,private cookieService: CookieService, private authService: AuthService) {        
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        
        if(this.cookieService.check(environment.authToken)){
            let token = this.cookieService.get(environment.authToken);
            var decoded = jwt_decode(token);
            const exp = decoded['exp'] * 1000;
            const current = new Date();
            const currentTimeStamp = (current.getTime());
            if( exp - 60000 <= currentTimeStamp  && currentTimeStamp < exp){
                const refreshCode = {
                    refresh_token : this.cookieService.get(environment.refreshToken)
                };

                this.authService.refresh(refreshCode).subscribe(
                    res => {
                        let newToken = res.token;
                        this.cookieService.set(environment.authToken,newToken);
                        const cloneRequest= req.clone({
                            headers: req.headers.set('Authorization','Bearer '+newToken)
                        });   
                        return next.handle(cloneRequest).pipe(
                            tap(
                                succ =>{},
                                err=>{
                                    if(err.status == 401){
                                        this.cookieService.delete(environment.authToken);
                                        this.router.navigate([environment.loginPath]);
                                    }else if(err.status == 403){
                                        this.router.navigate([environment.homePath])
                                    }
                                }
                            )
                        );
                    }
                );
            }else {
                const cloneRequest= req.clone({
                    headers: req.headers.set(environment.authToken,token)
                });   
                return next.handle(cloneRequest).pipe(
                    tap(
                        succ =>{},
                        err=>{
                            if(err.status == 401){
                                this.cookieService.delete(environment.authToken);
                                this.router.navigate([environment.loginPath]);
                            }else if(err.status == 403){
                                this.router.navigate([environment.homePath])
                            }
                        }
                    )
                );
            }
        }else{
            return next.handle(req.clone())
        }
    };
}