import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BaseService } from '../base.service';

@Injectable({
  providedIn: 'root'
})
export class BrandService  extends BaseService{

  routeApi = environment.apiUrlBrand;
}
