import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor(private toastController:ToastController) { }

  async presentToast(infoMessage: string,color :string,duration:number){
    const toast = await this.toastController.create({
      message: infoMessage,
      duration: duration,
      position:"top",
      color: color,
      
    });
    toast.present();
  }
}
