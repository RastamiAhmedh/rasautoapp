import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import { Router } from "@angular/router";
import {CookieService} from "ngx-cookie-service";

@Injectable({providedIn: "root"})
export class BaseService {
  httpOptions = {
    headers: new HttpHeaders({"Content-Type": "application/json"})
  };
  routeApi: string;
  classe;

  constructor(
    public http : HttpClient, 
    public cookieService : CookieService,
    public  router:  Router
  ) {}

    post(data:any){
      return this.http.post(this.routeApi,data,this.httpOptions)
    }

    getAll(){
      return this.http.get(this.routeApi,this.httpOptions)
    }

    get(id : number){
      return this.http.get(this.routeApi + id,this.httpOptions)
    }

    update(objet){
      return this.http.put(this.routeApi + objet.id,objet)
    }
}
