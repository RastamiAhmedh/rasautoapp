import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginUser } from 'src/app/models/login-user';
import { RefreshToken } from 'src/app/models/refresh_token';
import { Token } from 'src/app/models/token';
import { environment } from 'src/environments/environment';
import { BaseService } from '../base.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {
  
  login(LoginUser: LoginUser): Observable<Token>{
    return this.http.post<Token>(environment.apiUrlLogin,LoginUser,this.httpOptions)
  }

  refresh(refreshToken: RefreshToken): Observable<any>{
    return this.http.post<any>(environment.apiUrlRefreshToken,refreshToken,this.httpOptions);
  }

  isLogged(){
    if(this.cookieService.check(environment.authToken)){
      return true
    }else{
      return false
    }
  }
  
  logout(){
    this.cookieService.deleteAll();
    this.router.navigateByUrl("/"+environment.loginPath);
  }
  
}
