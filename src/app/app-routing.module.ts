import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthGuard } from './security/auth.guard';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: environment.homePath,
    pathMatch: 'full'
  },
  {
    path: environment.loginPath,
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'professional',
    loadChildren: () => import('./pages/professional/professional.module').then( m => m.ProfessionalPageModule),
    canActivate: [AuthGuard]
    
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'garage',
    loadChildren: () => import('./pages/garage/garage.module').then( m => m.GaragePageModule),
    
  },
  {
    path: 'advertisements',
    loadChildren: () => import('./pages/advertisements/advertisements.module').then( m => m.AdvertisementsPageModule)
  },
  {
    path: 'drivers',
    loadChildren: () => import('./pages/drivers/drivers.module').then( m => m.DriversPageModule)
  },
  {
    path: 'car',
    loadChildren: () => import('./pages/car/car.module').then( m => m.CarPageModule)
  },
  {
    path: 'choices',
    loadChildren: () => import('./pages/choices/choices.module').then( m => m.ChoicesPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
