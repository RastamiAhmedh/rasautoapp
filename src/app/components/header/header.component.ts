import {Component, Input, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import {AuthService} from "src/app/services/auth/auth.service";
import { environment } from "src/environments/environment";

@Component({selector: "app-header", templateUrl: "./header.component.html", styleUrls: ["./header.component.scss"]})
export class HeaderComponent implements OnInit {
  @Input()titleView: string;
  isLogged: boolean;
  constructor(private authService : AuthService, private router : Router,private cookieService:CookieService) {}

  ngOnInit() {
    this.isLogged = this.cookieService.check(environment.authToken);
  }
  logout() {
    this.authService.logout();
  }
  goRegister() {
    this.router.navigate(["/register"]);
  }
  login() {
    this.router.navigate(["/login"]);
  }
  backHome(){
    this.router.navigate(['/home']);
  }
}
