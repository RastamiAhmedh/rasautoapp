import { Component, Input, OnInit } from '@angular/core';
import { DriverService } from 'src/app/services/driver/driver.service';
import { ProfessionalService } from 'src/app/services/professional/professional.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  @Input() typeUser:string;
  constructor(private professionalService: ProfessionalService, private driverService: DriverService) { }

  ngOnInit() {}

}
