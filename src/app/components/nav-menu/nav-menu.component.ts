import {Component, OnInit} from "@angular/core";
import {CookieService} from "ngx-cookie-service";
import {environment} from "src/environments/environment";
import jwt_decode from "jwt-decode";
import {DecodeToken} from "src/app/models/decode-token";
import {MenuController} from "@ionic/angular";
import {AuthService} from "src/app/services/auth/auth.service";

@Component({selector: "app-nav-menu", templateUrl: "./nav-menu.component.html", styleUrls: ["./nav-menu.component.scss"]})
export class NavMenuComponent implements OnInit {
  roleUserConnected: string;
  decodedToken: DecodeToken;
  constructor(private cookieService : CookieService, private menu : MenuController, private autheService : AuthService) {}

  ngOnInit() {
    if(this.autheService.isLogged()){
      const token = this.cookieService.get(environment.authToken);
      const decodedToken: DecodeToken = jwt_decode(token);
      this.roleUserConnected = decodedToken.roles[0];
    }
  }

  isAdmin(): boolean {
    return this.roleUserConnected == environment.roleAdmin
      ? true
      : false;
  }
  isProfesssional(): boolean {
    return this.roleUserConnected == environment.roleProfessional
      ? true
      : false;
  }
  isDriver(): boolean {
    return this.roleUserConnected == environment.roleDriver
      ? true
      : false;
  }
}
